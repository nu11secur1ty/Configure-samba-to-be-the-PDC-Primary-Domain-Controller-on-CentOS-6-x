# [root@nu11secur1ty ~]#


```
1. yum install samba samba-client samba-common -y
```

```
2. vim /etc/samba/smb.conf
   - look and make copy fo the smb.conf
```

```
3. cp /etc/samba/smb.conf smb.conf.back
```

```
4. rm -rf /etc/samba/smb.conf
```


```
5. vim /etc/samba/smb.conf
   - add: 
        workgroup = test.com
        server string = test
        
        dns proxy = no
        client ntlmv2 auth = yes
        wins support = yes
        wins proxy = no
        lanman auth = yes
        ntlm auth = yes
        name resolve order = host wins lmhosts bcast
         
        log file = /var/log/samba/log.%m
        max log size = 1000 
        syslog = 0
        panic action = /usr/share/samba/panic-action %d
      
        security = user
        encrypt passwords = true
        passdb = tdbsam
        obey pam restrictions = yes
        unix password sync = yes
        
        domain logons = yes
        domain master = yes
        local master = yes
        preferred master = yes
        os level = 64
        logon path = \\%N\%U\profile
        logon drive = H:
        logon home = \\%N\%U
        
        add machine script = /usr/sbin/useradd -g machines -c "%u machine account" -d /var/lib/samba -s /bin/false %u

        [homes]
               comment = Network Logon Service
               path = /samba/netlogon
               browseable = no
               share modes = no
               guest ok = yes
               read only = yes
               
        [profiles]
                comment = Roaming profiles
                path = /samba/profiles
                guest ok = no
                browseable = no
                create mask = 0600
                directory mask = 0700
```                
:wq



```
7. cp /etc/samba/smb.conf smb.configured.bak
```


```
   - check:
8. groupdel machines
```


```
9. useradd user1
```

```
10. useradd user2
```

```
11. useradd user3
```

```
12. passwd user1 
    New password:
    - for user 2, 3
```

```
13. smbpasswd -a user1
    New SMB password:
    - for user 2, 3
```

```
14. smbpasswd -a root
    New SMB password:
```

```
15. [root@nu11secur1ty ~]# cd samba/
    ls -l
    mkdir -p /samba/netlogon
    mkdir -p /samba/profiles
    cd 
    chmod 777 /samba -R
    cd samba/
    ls -l
```

```    
16. service smb status
```

```
17. service nmb status
```

```
18. service smb start
```

```
19. service smb restart
```

```
20. service nmb start
```

```
21. service nmb restart
```

```
22. chkconfig smb on
```

```
23. chkconfig nmb on                  
```

```        
Going on windows machine:
  the computer name is: windowsxp(your name)
  In a member of, set Domain to: test.com
```
```
25. In login prompt type:
    user: root
    password: your password
    - Welcome in to test.com
```  
      
     

![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6.x/blob/master/2000px-Logo_Samba_Software.svg.png)

# Have fun with nu11secur1ty =)



