
![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6.x/blob/master/2000px-Logo_Samba_Software.svg.png)

# installation steps

Install all dependencies package to build system samba-4

    [root@server ~]# yum install glibc glibc-devel gcc python* libacl-devel krb5-workstation krb5-libs pam_krb5 libldap2-dev openldap-devel wget

Query old available rpm package of samba

    [root@server ~]# rpm -qa | grep samba

If package are available remove them with YUM

    [root@server ~]# yum remove samba-winbind-client samba-common samba-client

Install git to dowload the latest Samba 4 version

    [root@server ~]# yum install git-core

Download the latest version of samba from git repository

    [root@server ~]# git clone git://git.samba.org/samba.git samba-master

Reboot the server as a precaution so that all packages or kernel updates will be applied

    [root@server ~]#shutdown -r now

Login again as root to build samba and then compile samba package

    [root@server ~]#cd samba-master
    [root@server ~]#./configure –enable-debug –enable-selftest
    [root@server ~]#make

If everything reports okay you can then install samba

    [root@server ~]#make install

You should now have samba installed to ‘/usr/local/samba‘

Provision Samba 4 steps: The provision step sets up a basic user database, and is used when you are setting up your Samba4 server in its own domain.

Issue this command from root:

    [root@server ~]# /usr/local/samba/bin/samba-tool domain provision
    Realm [KVIT.IN]: (press Enter)
    Domain [KVIT]: (press Enter)
    Server Role (dc, member, standalone) [dc]: (press Enter)
    DNS backend (SAMBA_INTERNAL, BIND9_FLATFILE, BIND9_DLZ, NONE) [SAMBA_INTERNAL]: (press Enter)
    DNS forwarder IP address (write ‘none’ to disable forwarding) [8.8.8.8]: (press Enter)
    Administrator password:********* (Set Here DC Administrator password)
    Retype password: *********

Result will show like this

    Creating CN=MicrosoftDNS,CN=System,DC=kvit,DC=in
    Creating DomainDnsZones and ForestDnsZones partitions
    Populating DomainDnsZones and ForestDnsZones partitions
    Setting up sam.ldb rootDSE marking as synchronized
    Fixing provision GUIDs
    A Kerberos configuration suitable for Samba 4 has been generated at /usr/local/samba/private/krb5.conf
    Once the above files are installed, your Samba4 server will be ready to use
    Server Role: active directory domain controller
    Hostname: server
    NetBIOS Domain: KVIT
    DNS Domain: kvit.in
    DOMAIN SID: S-1-5-21-1541916875-3099074859-2332263890

NOTE: You may need to remove the ‘/usr/local/samba/etc/smb.conf’ file if you are re-running the provision command. If you encounter any errors when running the provision command, you may need to install the necessary missing packages or fix errors and then run ‘./configure’, ‘make’ and ‘make install’ commands again as stated above. Remember to do a ‘make clean’ in the root of your ‘samba-master’ directory before running ‘make’ again.

If the provision setup was successful reboot the server

    [root@server ~]#shutdown -r now

Start samba daemon manually

    [root@server ~]# /usr/local/samba/sbin/samba

If you like to start samba at boot time, Add ‘/usr/local/samba/sbin/samba’ in /etc/rc.local file.

    [root@server ~]#vim /etc/rc.local
    /usr/local/samba/sbin/samba

Verify you are indeed running the correct version

    [root@server ~]# /usr/local/samba/sbin/samba -V
    Version 4.3.0pre1-GIT-d098b6c

Now run this command to list the shares on your Samba4 server

    [root@server ~]# /usr/local/samba/bin/smbclient -L localhost -U%
    Domain=[KVIT] OS=[Windows 6.1] Server=[Samba 4.3.0pre1-GIT-d098b6c]
    Sharename Type Comment
    ——— —- ——-
    netlogon Disk
    sysvol Disk
    IPC$ IPC IPC Service (Samba 4.3.0pre1-GIT-d098b6c)
    Domain=[KVIT] OS=[Windows 6.1] Server=[Samba 4.3.0pre1-GIT-d098b6c]
    Server Comment
    ——— ——-
    Workgroup Master
    ——— ——-

# Configure DNS Now

Interface settings

    [root@server ~]# vim /etc/sysconfig/network-scripts/ifcfg-eth0
    DEVICE=”eth0″
    BOOTPROTO=”static”
    ONBOOT=”yes”
    TYPE=”Ethernet”
    GATEWAY=”192.168.0.254″
    IPADDR=”192.168.0.69″
    NETMASK=255.255.255.0

Edit your ‘/etc/resolv.conf‘ file

    [root@server ~]# vim /etc/resolv.conf
    domain kvit.in
    nameserver 192.168.0.69

Here is configuration file of samba 4 ‘/usr/local/samba/etc/smb.conf‘

    [root@server ~]# cat /usr/local/samba/etc/smb.conf
    # Global parameters
    [global]
    workgroup = KVIT
    realm = KVIT.IN
    netbios name = SERVER
    server role = active directory domain controller
    dns forwarder = 8.8.8.8
    [netlogon]
    path = /usr/local/samba/var/locks/sysvol/kvit.in/scripts
    read only = No
    [sysvol]
    path = /usr/local/samba/var/locks/sysvol
    read only = No

Test DNS Now

    [root@server ~]# nslookup kvit.in
    Server: 192.168.0.69
    Address: 192.168.0.69#53
    Name: kvit.in
    Address: 192.168.0.69

    [root@server ~]# host -t SRV _ldap._tcp.kvit.in.
    _ldap._tcp.kvit.in has SRV record 0 100 389 server.kvit.in.

    [root@server ~]# host -t SRV _kerberos._udp.kvit.in.
    _kerberos._udp.kvit.in has SRV record 0 100 88 server.kvit.in.

    [root@server ~]# host -t A server.kvit.in.
    server.kvit.in has address 192.168.0.69

Now, Flush and save Iptables

    [root@server ~]# iptables -F
    [root@server ~]# service iptables save
    [root@server ~]# service iptables restart

Configure Kerberos file

In CentOS 6.3 or 6.4, kerberos is handled by the ‘/etc/krb5.conf’ file. Make a backup copy of this original file, and then replace the existing file, if any, with the sample from /usr/local/samba/share/setup/krb5.conf.

    [root@server ~]# cp /usr/local/samba/share/setup/krb5.conf /etc/krb5.conf
    Edit file like below:
    [root@server ~]# cat /etc/krb5.conf
    [libdefaults]
    default_realm = KVIT.IN
    dns_lookup_realm = false
    dns_lookup_kdc = true

Testing Kerberos

    [root@server ~]# kinit administrator@KVIT.IN

‘kinit’ will not give you any output. To verify that Kerberos is working, and that you received a ticket, run the following

    [root@server ~]# klist
    Ticket cache: FILE:/tmp/krb5cc_0
    Default principal: administrator@KVIT.IN

    Valid starting     Expires            Service principal
    04/26/15 01:04:46  04/26/15 11:04:46  krbtgt/KVIT.IN@KVIT.IN
    renew until 04/27/15 01:04:41

# NTP (Network Time Protocol)

Make sure that ‘ntpd’ is running and installed. If ‘ntpd’ is not installed you can install it with YUM:

    [root@server ~]# yum install ntp
    [root@server ~]#/etc/init.d/ntpd start

Also, use the ‘chkconfig’ command to have ntpd run at boot:

    [root@server ~]#chkconfig ntpd on

    [root@server ~]# /usr/local/samba/sbin/samba

Put samba 4 command in bashrc

    [root@server ~]# vi ~/.bashrc
    export PATH=$PATH:/usr/local/samba/sbin:/usr/local/samba/bin

Reboot the the server to update bashrc file

or,

    [root@server ~]# bash

now restart samba daemon like this

    [root@server ~]# samba

Check the process of samba service

    [root@server ~]# ps -ef | grep samba

# Join windows system in Domain

Client machine settings

IP address settings of your Windows 7 client NIC

![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6/blob/master/sshot/ip-settings1.png)

Click ‘OK’ to save the changes.

Now bring up a command prompt in windows and ping the Samba DC:

ping 192.168.0.69

Verify that DNS is working correctly by pinging the FQDN:

ping server.kvit.in

If you get replies from both then this is a good sign and should mean that your Samba DC is functional. Also, you may need to reboot Windows for network settings to take effect.

# Configure data and time on client system
![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6/blob/master/sshot/Date-and-time.png)

# Join windows client in to domain

1). Right-click ‘My Computer’ icon and choose ‘Properties’

2). From the left-side pane click ‘Advanced system settings’

3). Choose the ‘Computer Name’ tab and click ‘Change…’

4). Select option ‘Domain’, and insert KVIT.IN. If this fails just try KVIT

![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6/blob/master/sshot/join-in-domain.png)

5). When it requests a username and password, type ‘Administrator’ as the username and then enter your password. (password = the password you used when you ran the ‘samba-tool domain provision’ command)

6). You should get a message box stating ‘Welcome to the KVIT.IN domain’

7). Click OK on this message box and the Properties window, and you will then be instructed to restart your computer.

8). After restarting you should be presented with the normal login dialog. Click on ‘Switch User’ button.

9). Choose ‘Other user’ and then enter in the following.

![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6/blob/master/sshot/login.png)

Press ‘Enter’ or the arrow button.

10). You should then authenticate and then login to Windows.

# Install windows Remote Administration toll

To install the GUI tools to manage the domain you must install the Remote Server Administration Tools . This will allow you easily manage the domain using Active Directory.

11). Download form link: http://www.microsoft.com/en-us/download/details.aspx?id=7887

12).Enable the necessary components in ‘Control Panel -> Programs -> Turn Windows features on or off -> Remote Server Administration Tools’

![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6/blob/master/sshot/Enable-remote-tool.png)

13). Now open AD Console and create one or more domain user

Go to Run type : dsa.msc ( For AD console)

![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6/blob/master/sshot/AD-Console.png)

14). Now create a domain user to login in to system

R/c on User ==> New ==> User:

First Name: Your name

Last Name: Your name

User Logon name: nu11secur1ty

Set user password: ******* (Create strong password using letters and symbols)

# Now, login in to client machine with new domain user

![image](https://github.com/nu11secur1ty/Configure-samba-to-be-the-PDC-Primary-Domain-Controller-on-CentOS-6/blob/master/sshot/local-user-login.png)

# Have fun with nu11secur1ty =)
